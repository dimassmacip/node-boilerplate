import { MongoClient } from "mongodb"
import { config } from "../../../config/config"

let client: MongoClient

export const getMongoClient = (): MongoClient => client

export const initMongo = () => {
  const url: string  = `mongodb://${config.database.mongo.host}:${config.database.mongo.port}`  
  client = new MongoClient(url)
  client.connect()
}
